/**
 * All these macros has to be defined in the makefile and
 * processed during the make process
 * */
#ifndef THERMOMETER_DEVDEFS_H
    #define THERMOMETER_DEVDEFS_H
    #ifndef ARDUINO
        #define ARDUINO 10809
    #endif
    
    // this has to be already defined during the make process
    #ifndef __AVR_ATmega328P__
        #define __AVR_ATmega328P__
    #endif

    // actual baud has to be defined during the make process
    #ifndef BAUD
        #define BAUD  9600 // 115200 
    #endif
#endif
