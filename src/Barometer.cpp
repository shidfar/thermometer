#include "Barometer.h"

namespace thermometer {
        Barometer::Barometer() {
            this->bmp280 = new Adafruit_BMP280();
            this->bmp280->begin();
        }
        
        Barometer::~Barometer() {
            delete bmp280;
        }

        void Barometer::get_latest_data(char * out) {
            float pressure;
            float temperature;
            uint16_t altitude;

            char str_pressure[8];
            char str_temperature[6];


            pressure = this->bmp280->readPressure() / 100;
            temperature = this->bmp280->readTemperature();
            altitude = this->bmp280->readAltitude(1016.0);

            dtostrf(pressure, 6, 1, str_pressure);
            dtostrf(temperature, 4, 1, str_temperature);
            
            sprintf(out, "Temperature: %s %cC\nPressure: %s hPa\nAltitude: %im", str_temperature, 248, str_pressure, altitude);
        }
}