#include <stdio.h>

#include "DevDefs.h" // only for dev purposes

#include <util/delay.h>
#include <avr/io.h>
#include <util/setbaud.h>

#ifdef __cplusplus
    extern "C" {
        FILE * uart_str_stream;
    }
#endif

void initialize_uart(void);
int uart_putchar(char, FILE *);
int uart_getchar(FILE *);

void initialize_uart(void) {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

    #if USE_2X
        UCSR0A |= _BV(U2X0);
    #else
        UCSR0A &= ~(_BV(U2X0));
    #endif

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */

    uart_str_stream = fdevopen(uart_putchar, uart_getchar);
    stdout = stdin = uart_str_stream;
}

int uart_putchar(char c, FILE *stream) {
    if (c == '\n') {
        uart_putchar('\r', stream);
    }
    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = c;
    return 0;
}

int uart_getchar(FILE *stream) {
    loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
    return UDR0;
}

int destroy_uart() {
    delete uart_str_stream;
}
