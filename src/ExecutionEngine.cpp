#include "ExecutionEngine.hpp"

namespace thermometer
{
    ExecutionEngine::ExecutionEngine() {
        lcd_screen = new LcdWrapper();
        // temperature = new Temperature();
        barometer = new Barometer();
    }
    
    void ExecutionEngine::routine(void) {
        // char ts_out[20];
        char bmp_out[80];
        // char combined[100];

        // temperature->get_latest(ts_out);
        barometer->get_latest_data(bmp_out);
        // sprintf(combined, "%s\n%s", ts_out, bmp_out);
        lcd_screen->write_to_oled(bmp_out);
    }

    void ExecutionEngine::run(void) {
        while (true) {
            routine();
            _delay_ms(60000);
        }
    }

    ExecutionEngine::~ExecutionEngine() {
        delete lcd_screen;
        // delete temperature;
    }
    
} // namespace dolorest
