#ifndef EXECUTION_ENGINE_H
    #define EXECUTION_ENGINE_H

    #include "DevDefs.h"
    #include "GlobalDefs.h"

    #include <stdio.h>
    #include <util/delay.h>
    #include <avr/io.h>
    #include <util/setbaud.h>

    #include "LcdWrapper.hpp"
    // #include "Temperature.hpp"
    #include "Barometer.h"

    namespace thermometer {
        class ExecutionEngine
        {
        private:
            inline void routine(void);
            LcdWrapper *lcd_screen;
            // Temperature *temperature;
            Barometer *barometer;

        public:
            ExecutionEngine();
            ~ExecutionEngine();
            void run(void);
        };
    };
#endif