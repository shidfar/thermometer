#ifndef BAROMETER_H
    #define BAROMETER_H

    #include <Wire.h>
    #include <Adafruit_Sensor.h>
    #include <Adafruit_BMP280.h>

    namespace thermometer {
        class Barometer
        {
        private:
            Adafruit_BMP280 * bmp280;
        public:
            Barometer();
            ~Barometer();
            void get_latest_data(char *);
        };
    }
#endif
