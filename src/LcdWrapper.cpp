#include "LcdWrapper.hpp"

namespace thermometer {
    LcdWrapper::LcdWrapper() {
        #ifdef LCDI2C
            this->lcd = new LiquidCrystal_I2C(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
            this->lcd->begin(LCD_W, LCD_H);
        #else
            this->oled = new Adafruit_SSD1306(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
            if(!this->oled->begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
                // Serial.println(F("SSD1306 allocation failed"));
                for(;;); // Don't proceed, loop forever
            }
            this->oled->display();
        #endif
    }
    
    LcdWrapper::~LcdWrapper() {
        #ifdef LCDI2C
            delete lcd;
        #else
            delete oled;
        #endif
    }

    #ifdef LCDI2C
        void LcdWrapper::writetolcd32(int w, char src[]) {
            char dest[16];
            this->lcd->clear();
            this->lcd->setCursor(0, 0);
            strncpy(dest, src, w);
            this->lcd->print(dest);
            if (strlen(src) > 16) {
                this->lcd->setCursor(0, 1);
                strncpy(dest, src + w, w);
                this->lcd->print(dest);
            }
        }

        void LcdWrapper::writetolcd(int w, char src[]) {
            this->lcd->setCursor(0, 0);
            char dest[16];
            int c = 0;
            for (int i = 0; i <= (strlen(src) / w); i++) {
                this->lcd->clear();
                this->lcd->setCursor(0, 0);
                strncpy(dest, src + c, w);
                this->lcd->print(dest);
                this->lcd->setCursor(0, 1);
                strncpy(dest, src + w + c, w);
                this->lcd->print(dest);
                c += w;
                delay(2500);
            }
        }
    #else
        void LcdWrapper::write_to_oled(char src[]) {
            this->oled->clearDisplay();

            this->oled->setTextSize(1);      // Normal 1:1 pixel scale
            this->oled->setTextColor(SSD1306_WHITE); // Draw white text
            this->oled->setCursor(0, 0);     // Start at top-left corner
            this->oled->cp437(true);         // Use full 256 char 'Code Page 437' font

            // Not all the characters will fit on the display. This is normal.
            // Library will draw what it can and the rest will be clipped.

            uint16_t len = strlen(src);

            for (int16_t i = 0; i < len; i++) {
                this->oled->write(src[i]);
            }

            this->oled->display();
        }
    #endif
}
