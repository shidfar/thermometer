#ifndef LCD_WRAPPER_H
    #define LCD_WRAPPER_H

    #include "GlobalDefs.h"
    #include "DevDefs.h"
#ifdef LCDI2C
    #include <LiquidCrystal_I2C.h>
#else
    #include <Wire.h>
    #include <Adafruit_GFX.h>
    #include <Adafruit_SSD1306.h>
#endif

    namespace thermometer {
        class LcdWrapper
        {
        private:
        #ifdef LCDI2C
            LiquidCrystal_I2C * lcd;
        #else
            Adafruit_SSD1306 * oled;
        #endif
            
        public:
            LcdWrapper();
            ~LcdWrapper();
        #ifdef LCDI2C
            void writetolcd32(int w, char src[]);
            void writetolcd(int w, char src[]);
        #else
            void write_to_oled(char src[]);
        #endif
        };
    }
#endif
